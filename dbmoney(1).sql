-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-04-2019 a las 02:45:16
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbmoney`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `account`
--

CREATE TABLE `account` (
  `AccountId` int(5) NOT NULL,
  `UserId` int(5) NOT NULL,
  `AccountName` varchar(255) CHARACTER SET latin1 NOT NULL,
  `Level` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `account`
--

INSERT INTO `account` (`AccountId`, `UserId`, `AccountName`, `Level`) VALUES
(14, 4, 'Efectivo en caja y banco', 1),
(15, 4, 'Cuentas por cobrar', 1),
(16, 4, 'Inventarios', 1),
(17, 4, 'Gastos pagados por anticipado', 1),
(18, 4, 'Activo Fijo', 1),
(19, 4, 'Depreciacion', 1),
(20, 4, 'Otros Activos', 1),
(21, 4, 'Capital', 2),
(22, 4, 'Cuentas por pagar', 2),
(23, 4, 'Prestamos por pagar', 2),
(24, 4, 'Tarjetas de Creditos', 2),
(25, 4, 'Acumulaciones', 2),
(26, 4, 'Deudas a largo plazo', 2);

--
-- Disparadores `account`
--
DELIMITER $$
CREATE TRIGGER `GenerateAccount` AFTER INSERT ON `account` FOR EACH ROW INSERT INTO totals(UserId, AccountId, totals) values (new.UserId, new.AccountId, '0')
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `assets`
--

CREATE TABLE `assets` (
  `AssetsId` int(5) NOT NULL,
  `UserId` int(5) NOT NULL,
  `Title` varchar(255) CHARACTER SET latin1 NOT NULL,
  `Date` date NOT NULL,
  `CategoryId` int(5) NOT NULL,
  `AccountId` int(5) NOT NULL,
  `Amount` varchar(255) CHARACTER SET latin1 NOT NULL,
  `Description` text CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `assets`
--

INSERT INTO `assets` (`AssetsId`, `UserId`, `Title`, `Date`, `CategoryId`, `AccountId`, `Amount`, `Description`) VALUES
(1, 4, 'Incentivo', '2019-03-27', 62, 14, '2000', 'dinero extra'),
(2, 4, 'pago de amazon prime', '2019-03-30', 62, 14, '1000', ''),
(3, 4, 'jeepeta', '2019-03-30', 63, 18, '200000', ''),
(4, 4, 'otra jeepeta', '2019-03-30', 63, 19, '1000', ''),
(5, 4, 'faf', '2019-03-30', 62, 15, '20', ''),
(6, 4, 'otro', '2019-03-30', 64, 20, '2000', ''),
(7, 4, 'fsd', '2019-04-03', 63, 16, '7888', '');

--
-- Disparadores `assets`
--
DELIMITER $$
CREATE TRIGGER `GenerateTotalAccount` AFTER INSERT ON `assets` FOR EACH ROW UPDATE totals SET totals.totals=totals.totals + new.amount where totals.userid=new.userid and totals.accountid=new.accountid
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `GenerateTotalUpdate` AFTER UPDATE ON `assets` FOR EACH ROW UPDATE totals SET totals.totals=(select sum(Amount) from assets where assets.userid=new.userid and assets.accountid=new.accountid) where totals.userid=new.userid and totals.accountid=new.accountid
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bills`
--

CREATE TABLE `bills` (
  `BillsId` int(5) NOT NULL,
  `UserId` int(5) NOT NULL,
  `Title` varchar(255) CHARACTER SET latin1 NOT NULL,
  `Dates` date NOT NULL,
  `CategoryId` int(5) NOT NULL,
  `AccountId` int(5) NOT NULL,
  `Amount` varchar(255) CHARACTER SET latin1 NOT NULL,
  `Description` text CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `bills`
--

INSERT INTO `bills` (`BillsId`, `UserId`, `Title`, `Dates`, `CategoryId`, `AccountId`, `Amount`, `Description`) VALUES
(1, 4, 'Luz', '2019-03-27', 65, 22, '2000', 'se necesita pagar la luz'),
(2, 4, 'materiales de oficina ', '2019-03-29', 65, 22, '400', ''),
(3, 4, 'escuela', '2019-03-31', 65, 25, '200', ''),
(4, 4, 'afdsadsf', '2019-04-03', 65, 22, '224455', '');

--
-- Disparadores `bills`
--
DELIMITER $$
CREATE TRIGGER `GenerateExpense` AFTER INSERT ON `bills` FOR EACH ROW UPDATE totals SET totals.totals=totals.totals - new.amount where totals.userid=new.userid and totals.accountid=new.accountid
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `budget`
--

CREATE TABLE `budget` (
  `BudgetId` int(5) NOT NULL,
  `UserId` int(5) NOT NULL,
  `CategoryId` int(5) NOT NULL,
  `Dates` date NOT NULL,
  `Amount` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `budget`
--

INSERT INTO `budget` (`BudgetId`, `UserId`, `CategoryId`, `Dates`, `Amount`) VALUES
(1, 3, 34, '2019-03-22', 1000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category`
--

CREATE TABLE `category` (
  `CategoryId` int(5) NOT NULL,
  `UserId` int(5) NOT NULL,
  `CategoryName` varchar(255) CHARACTER SET latin1 NOT NULL,
  `Level` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `category`
--

INSERT INTO `category` (`CategoryId`, `UserId`, `CategoryName`, `Level`) VALUES
(62, 4, 'Activo Corriente', 1),
(63, 4, 'Activos Fijos', 1),
(64, 4, 'Otros Activos', 1),
(65, 4, 'Pasivo Corriente', 2),
(66, 4, 'Capital', 2),
(67, 4, 'Pasivo No Corriente', 2),
(68, 1, 'Salary', 1),
(69, 1, 'Alowance', 1),
(70, 1, 'Petty Cash', 1),
(71, 1, 'Bonus', 1),
(72, 1, 'Food', 2),
(73, 1, 'Social Life', 2),
(74, 1, 'Self-Development', 2),
(75, 1, 'Transportation', 2),
(76, 1, 'Culture', 2),
(77, 1, 'Household', 2),
(78, 1, 'Apparel', 2),
(79, 1, 'Beauty', 2),
(80, 1, 'Health', 2),
(81, 1, 'Gift', 2),
(82, 2, 'Salary', 1),
(83, 2, 'Alowance', 1),
(84, 2, 'Petty Cash', 1),
(85, 2, 'Bonus', 1),
(86, 2, 'Food', 2),
(87, 2, 'Social Life', 2),
(88, 2, 'Self-Development', 2),
(89, 2, 'Transportation', 2),
(90, 2, 'Culture', 2),
(91, 2, 'Household', 2),
(92, 2, 'Apparel', 2),
(93, 2, 'Beauty', 2),
(94, 2, 'Health', 2),
(95, 2, 'Gift', 2),
(96, 5, 'Salary', 1),
(97, 5, 'Alowance', 1),
(98, 5, 'Petty Cash', 1),
(99, 5, 'Bonus', 1),
(100, 5, 'Food', 2),
(101, 5, 'Social Life', 2),
(102, 5, 'Self-Development', 2),
(103, 5, 'Transportation', 2),
(104, 5, 'Culture', 2),
(105, 5, 'Household', 2),
(106, 5, 'Apparel', 2),
(107, 5, 'Beauty', 2),
(108, 5, 'Health', 2),
(109, 5, 'Gift', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `totals`
--

CREATE TABLE `totals` (
  `TotalsId` int(5) NOT NULL,
  `UserId` int(5) NOT NULL,
  `AccountId` int(5) NOT NULL,
  `Totals` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `totals`
--

INSERT INTO `totals` (`TotalsId`, `UserId`, `AccountId`, `Totals`) VALUES
(14, 4, 14, 3000),
(15, 4, 15, 20),
(16, 4, 16, 7888),
(17, 4, 17, 0),
(18, 4, 18, 200000),
(19, 4, 19, 1000),
(20, 4, 20, 2000),
(21, 4, 21, 0),
(22, 4, 22, -226855),
(23, 4, 23, 0),
(24, 4, 24, 0),
(25, 4, 25, -200),
(26, 4, 26, 0),
(27, 0, 27, 0),
(28, 0, 28, 0),
(29, 4, 29, 0),
(30, 4, 30, 0),
(31, 4, 31, 0),
(32, 4, 32, 0),
(33, 4, 33, 0),
(34, 4, 34, 0),
(35, 4, 35, 0),
(36, 4, 36, 0),
(37, 4, 37, 0),
(38, 4, 38, 0),
(39, 4, 39, 0),
(40, 4, 40, 0),
(41, 5, 41, 0),
(42, 5, 42, 0),
(43, 5, 43, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `UserId` int(5) NOT NULL,
  `FirstName` varchar(255) CHARACTER SET latin1 NOT NULL,
  `LastName` varchar(255) CHARACTER SET latin1 NOT NULL,
  `Email` varchar(255) CHARACTER SET latin1 NOT NULL,
  `Password` varchar(255) CHARACTER SET latin1 NOT NULL,
  `Currency` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`UserId`, `FirstName`, `LastName`, `Email`, `Password`, `Currency`, `Company`) VALUES
(1, 'admin', 'admin', 'a@example.com', 'LjQIdwGE9y5PgLMZdbn8WtWcH8LFTpnoJ1Kx7HnpCes=', '$', NULL),
(2, 'adrian', 'acosta', 'adriankenny01@gmail.com', 'RB7knZzaZYBtZefGcXfIw2Zo6id+MmBspAO+6eLKmWE=', 'RD$', NULL),
(3, 'prueba', 'apellido', 'b@example.com', 'RB7knZzaZYBtZefGcXfIw2Zo6id+MmBspAO+6eLKmWE=', '$', NULL),
(4, 'ca', 'ba', 'c@example.com', 'RB7knZzaZYBtZefGcXfIw2Zo6id+MmBspAO+6eLKmWE=', 'RD$', 'Sistema OYM'),
(5, 'j', 'ja', 'j@example.com', 'RB7knZzaZYBtZefGcXfIw2Zo6id+MmBspAO+6eLKmWE=', '$', 'ja y asociados');

--
-- Disparadores `user`
--
DELIMITER $$
CREATE TRIGGER `GenerateDefaultAccount` AFTER INSERT ON `user` FOR EACH ROW INSERT INTO account (UserId, AccountName) VALUES (new.UserId, 'Cash'), (new.UserId, 'Account'), (new.UserId, 'Card')
$$
DELIMITER ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`AccountId`);

--
-- Indices de la tabla `assets`
--
ALTER TABLE `assets`
  ADD PRIMARY KEY (`AssetsId`),
  ADD KEY `fk_test` (`AccountId`);

--
-- Indices de la tabla `bills`
--
ALTER TABLE `bills`
  ADD PRIMARY KEY (`BillsId`),
  ADD KEY `fk_testt` (`AccountId`);

--
-- Indices de la tabla `budget`
--
ALTER TABLE `budget`
  ADD PRIMARY KEY (`BudgetId`);

--
-- Indices de la tabla `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`CategoryId`);

--
-- Indices de la tabla `totals`
--
ALTER TABLE `totals`
  ADD PRIMARY KEY (`TotalsId`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`UserId`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `account`
--
ALTER TABLE `account`
  MODIFY `AccountId` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT de la tabla `assets`
--
ALTER TABLE `assets`
  MODIFY `AssetsId` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `bills`
--
ALTER TABLE `bills`
  MODIFY `BillsId` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `budget`
--
ALTER TABLE `budget`
  MODIFY `BudgetId` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `category`
--
ALTER TABLE `category`
  MODIFY `CategoryId` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;
--
-- AUTO_INCREMENT de la tabla `totals`
--
ALTER TABLE `totals`
  MODIFY `TotalsId` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `UserId` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `assets`
--
ALTER TABLE `assets`
  ADD CONSTRAINT `fk_test` FOREIGN KEY (`AccountId`) REFERENCES `account` (`AccountId`) ON DELETE CASCADE;

--
-- Filtros para la tabla `bills`
--
ALTER TABLE `bills`
  ADD CONSTRAINT `fk_testt` FOREIGN KEY (`AccountId`) REFERENCES `account` (`AccountId`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
