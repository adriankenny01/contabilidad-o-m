<?php

//Include Functions
include('includes/Functions.php');

//Include Notifications
include ('includes/notification.php');


$SearchTerm = '';

if (isset($_POST['submitin'])) {
	
		$IncomeIds = $_POST['incomeid'];	
		//Get Account Id
		$GetAccountId = "SELECT AccountId FROM assets WHERE UserId = $UserId and AssetsId = $IncomeIds";
		$AccountId = mysqli_query($mysqli,$GetAccountId);
		$ColId = mysqli_fetch_array($AccountId);
		$AccId = $ColId['AccountId'];
		//Delete Income
		$Delete = "DELETE FROM assets WHERE AssetsId = $IncomeIds";
		$DeleteI = mysqli_query($mysqli,$Delete); 
		
		//Update Total Income
		$TotalIncome = "UPDATE totals SET totals.Totals = (SELECT SUM(Amount) FROM assets where assets.UserId = $UserId AND assets.AccountId = totals.AccountId) \n"
    . "	WHERE totals.UserId = $UserId AND totals.AccountId = $AccId";
		$UpdateTotalIncome = mysqli_query($mysqli,$TotalIncome);
		if(!$UpdateTotalIncome){
			$Gagal="QUERY ERROR";
				 $msgBox = alertBox($Gagal);
			}
		//$msgBox = alertBox($AccId);
		$msgBox = alertBox($DeleteIncome);
	}
	
//Get Report Income History
$GetIncomeHistory = "SELECT * from assets left JOIN category on assets.CategoryId = category.CategoryId left JOIN account on assets.AccountId = account.AccountId where assets.UserId = $UserId ORDER BY assets.Date DESC";
$IncomeHistory = mysqli_query($mysqli,$GetIncomeHistory); 


// Get all by month Income
$GetAllIncomeDate 	 = "SELECT SUM(Amount) AS Amount FROM assets WHERE UserId = $UserId AND MONTH(Date) = MONTH (CURRENT_DATE())";
$GetAIncomeDate		 = mysqli_query($mysqli, $GetAllIncomeDate);
$IncomeColDate 		 = mysqli_fetch_assoc($GetAIncomeDate);

// Get all by today Income
$GetAllIncomeDateToday 		 = "SELECT SUM(Amount) AS Amount FROM assets WHERE UserId = $UserId AND Date = CURRENT_DATE()";
$GetAIncomeDateToday		 = mysqli_query($mysqli, $GetAllIncomeDateToday);
$IncomeColDateToday 		 = mysqli_fetch_assoc($GetAIncomeDateToday);

// Search Income
if (isset($_POST['searchbtn'])) {
	$SearchTerm = $_POST['search'];
	$GetIncomeHistory = "SELECT * from assets left JOIN category on assets.CategoryId = category.CategoryId left JOIN account on assets.AccountId = account.AccountId where 
					(assets.Title like '%$SearchTerm%' 
					OR account.AccountName like '%$SearchTerm%'
					OR assets.Description like '%$SearchTerm%' 
					OR category.CategoryName like '%$SearchTerm%')
					AND assets.UserId = $UserId ORDER BY assets.Date DESC";
$IncomeHistory = mysqli_query($mysqli,$GetIncomeHistory); 
	
}

// Get User Info
$GetUsers	 	 = "SELECT  Company from user WHERE UserId = $UserId";
$GetUserq		 = mysqli_query($mysqli, $GetUsers);
$UserInfos 		 = mysqli_fetch_assoc($GetUserq);

//Get assets
$GetAssetsCategory	 	 = "SELECT c.CategoryName as name FROM `assets` a RIGHT JOIN `category` c on a.CategoryId = c.CategoryId WHERE c.level = 1";
$GetAssetsCategoryq		 = mysqli_query($mysqli, $GetAssetsCategory);

//Get bills
$GetBillsCategory	 	 = "SELECT c.CategoryName as name FROM `bills` b RIGHT JOIN `category` c on b.CategoryId = c.CategoryId WHERE c.level = 2";
$GetBillsCategoryq		 = mysqli_query($mysqli, $GetBillsCategory);

//Get Activo Corriente
$GetActivosCorrienteC 	 = "SELECT CategoryName FROM `category` WHERE CategoryId = 62";
$GetActivosCorrienteQ	 = mysqli_query($mysqli, $GetActivosCorrienteC);
$ActivosQuery 	 = mysqli_fetch_assoc($GetActivosCorrienteQ);

//Get Activos
$GetActivos	 	 = "SELECT sum(a.amount) AS monto , ac.AccountName AS AccountName FROM `assets` a JOIN account ac ON a.AccountId = ac.AccountId WHERE a.accountId in (14,15,16,17) GROUP BY a.accountid";
$GetActivosq	 = mysqli_query($mysqli, $GetActivos);

//Get toatl Activos
$GetActivoCorrienteTotal 	 	= "SELECT sum(a.amount) AS monto FROM `assets` a JOIN account ac ON a.AccountId = ac.AccountId WHERE a.accountId in (14,15,16,17) ";
$GetActivoCorrienteTotalq	 	= mysqli_query($mysqli, $GetActivoCorrienteTotal);
$GetActivoCorrienteTotalQuery 	 = mysqli_fetch_assoc($GetActivoCorrienteTotalq);

//Get Activos Fijo Category
$GetActivoFijo 	 = "SELECT CategoryName FROM `category` where CategoryId = 63";
$GetActivoFijoq	 = mysqli_query($mysqli, $GetActivoFijo);
$ActivosFijoQuery 	 = mysqli_fetch_assoc($GetActivoFijoq);

//Get Activos Fijos
$GetActivosFijos = "SELECT sum(a.amount) AS monto , ac.AccountName AS AccountName FROM `assets` a JOIN account ac ON a.AccountId = ac.AccountId WHERE a.accountId IN (18,19) GROUP BY a.accountid";
$GetActivosFijosq	 = mysqli_query($mysqli, $GetActivosFijos);

//Get total Activos Fijos
$GetActivoNoCorrienteTotal 	 	= "SELECT sum(a.amount) AS monto FROM `assets` a JOIN account ac ON a.AccountId = ac.AccountId WHERE a.accountId in (18,19) ";
$GetActivoNoCorrienteTotalq	 	= mysqli_query($mysqli, $GetActivoNoCorrienteTotal);
$GetActivoNoCorrienteTotalQuery 	 = mysqli_fetch_assoc($GetActivoNoCorrienteTotalq);

//Get Otros Activo Category
$GetOtrosActivosc 	 = "SELECT CategoryName FROM `category` WHERE CategoryId = 64";
$GetOtrosActivosq	 = mysqli_query($mysqli, $GetOtrosActivosc);
$OtrosActivosQuery 	 = mysqli_fetch_assoc($GetOtrosActivosq);

//Get Otros Activos 
$GetOtrosActivos = "SELECT sum(a.amount) AS monto , ac.AccountName AS AccountName FROM `assets` a JOIN account ac ON a.AccountId = ac.AccountId WHERE a.accountId = 20 GROUP BY a.accountid";
$GetOtrosActivosq	 = mysqli_query($mysqli, $GetOtrosActivos);

//Get total Activos
$GetTotalActivos 	 = "SELECT sum(a.amount) AS monto FROM `assets` a JOIN account ac ON a.AccountId = ac.AccountId";
$GetTotalActivosq	 = mysqli_query($mysqli, $GetTotalActivos);
$GetTotalActivosQuery 	 = mysqli_fetch_assoc($GetTotalActivosq);



//Get Pasivo Corriente Category
$GetPasivoCorrientec 	 = "SELECT CategoryName FROM `category` where CategoryId = 65";
$GetPasivoCorrienteq	 = mysqli_query($mysqli, $GetPasivoCorrientec);
$GetPasivoCorrienteQuery 	 = mysqli_fetch_assoc($GetPasivoCorrienteq);

$GetPasivoCorriente = "SELECT sum(b.amount) AS monto , ac.AccountName AS AccountName FROM `bills` b JOIN account ac ON b.AccountId = ac.AccountId WHERE b.accountId in (22,23,24,25) GROUP BY ac.accountid";
$GetPasivoCorrienteQueryc	 = mysqli_query($mysqli, $GetPasivoCorriente);

$GetPasivoCorrienteTotal = "SELECT sum(b.amount) AS monto FROM `bills` b JOIN account ac ON b.AccountId = ac.AccountId WHERE b.accountId in (22,23,24,25)";
$GetPasivoCorrienteTotalc	 = mysqli_query($mysqli, $GetPasivoCorrienteTotal);
$GetPasivoCorrienteTotalQuery = mysqli_fetch_assoc($GetPasivoCorrienteTotalc);

//Get Pasivo No Corriente Category
$GetPasivoNoCorrientec 	 = "SELECT CategoryName FROM `category` where CategoryId = 67";
$GetPasivoNoCorrienteq	 = mysqli_query($mysqli, $GetPasivoNoCorrientec);
$GetPasivoNOCorrienteQuery 	 = mysqli_fetch_assoc($GetPasivoNoCorrienteq);

$GetPasivoNoCorriente = "SELECT * from account where accountid = 26";
$GetPasivoNoCorrienteQueryc	 = mysqli_query($mysqli, $GetPasivoNoCorriente);

$GetPasivoNoCorrienteTotal = "SELECT sum(b.amount) AS monto FROM `bills` b JOIN account ac ON b.AccountId = ac.AccountId WHERE b.accountId in (26)";
$GetPasivoNoCorrienteTotalc	 = mysqli_query($mysqli, $GetPasivoNoCorrienteTotal);
$GetPasivoNoCorrienteTotalQuery = mysqli_fetch_assoc($GetPasivoNoCorrienteTotalc);

//Get Capital Category
$GetCapitalc 	 = "SELECT CategoryName FROM `category` where CategoryId = 66";
$GetCapitalq	 = mysqli_query($mysqli, $GetCapitalc);
$GetCapitalQuery 	 = mysqli_fetch_assoc($GetCapitalq);

$GetCapital = "SELECT sum(b.amount) as monto , ac.AccountName as AccountName FROM `bills` b JOIN account ac on b.AccountId = ac.AccountId where b.accountId =  21";
$GetCapitalQueryc	 = mysqli_query($mysqli, $GetCapital);

$GetCapitalTotal	 = "SELECT CategoryName FROM `category` where CategoryId = 66";
$GetCapitaltotalq	 = mysqli_query($mysqli, $GetCapitalTotal);
$GetCapitalTotalQuery 	 = mysqli_fetch_assoc($GetCapitaltotalq);


//Pasivos Total
$GetPasivosTotal 	 = "SELECT sum(b.amount) as monto , ac.AccountName as AccountName FROM `bills` b JOIN account ac on b.AccountId = ac.AccountId";
$GetPasivosTotalq	 = mysqli_query($mysqli, $GetPasivosTotal);
$GetPasivosTotalQuery 	 = mysqli_fetch_assoc($GetPasivosTotalq);


//Total
$CountTotals = $GetPasivosTotalQuery['monto'] - $GetTotalActivosQuery['monto'];


//Include Global page
	include ('includes/global.php');
	
	
?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header text-center"><?php echo $ExpenseIncomeReports ;?></h1>
					<h3 class="text-center"><?php echo $UserInfos['Company']; ?></h3>
					<h3 class="text-center text-bold"> <?php $date = new DateTime(); echo $date->format('d-F-Y'); ?> </h3>   
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
				
                <div class="col-lg-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                           <i class="fa fa-plus"></i> <?php echo $Asset ;?>
                            
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
							     
                            <div class="">
                                <table class="table table-hover table-striped" id="assetsdata">
                                    <thead>
        			                <tr>
										<th class="text-left"><?php echo $Account ;?></th>
        			                    <th class="text-right"><?php echo $Amount ;?></th>
        			                </tr>
			                     </thead>

	                	<tbody>
							
								<tr>
								<th><?php echo $ActivosQuery['CategoryName']; ?></th>
								<th></th>
							</tr>
							<?php while($col = mysqli_fetch_assoc($GetActivosq)): ?>
							<tr>
								<td class="text-left"><?php echo $col['AccountName'];?></td>
								<td class="activo text-right"><?php echo number_format($col['monto']) ;?></td>
							</tr>
							<?php endwhile; ?>

							<tr>
								<td class="text-left"><i>Total de activos corrientes<i></td>
								<td class="text-right"><i><?php echo number_format($GetActivoCorrienteTotalQuery['monto']); ?><i></td>
							</tr>

							<tr>
								<th><?php echo $ActivosFijoQuery['CategoryName']; ?>
								<th></th>
							</tr>

							<?php while($col = mysqli_fetch_assoc($GetActivosFijosq)): ?>
							<tr>
								<td class="text-left"><?php echo $col['AccountName'];?></td>
								<td class="text-right"><?php echo number_format($col['monto']);?></td>
							</tr>
							<?php endwhile; ?>

							<tr>
								<td class="text-left"><i>Total de activos fijos<i></td>
								<td class="text-right"><i><?php echo number_format($GetActivoNoCorrienteTotalQuery['monto']); ?><i></td>
							</tr>

							<!-- otros activos -->
							<tr>
								<th><?php echo $OtrosActivosQuery['CategoryName']; ?></th>
								<th></th>
							</tr>

							<?php while($col = mysqli_fetch_assoc($GetOtrosActivosq)): ?>
							<tr>
								<td class="text-left"><?php echo $col['AccountName'];?></td>
								<td class="text-right"><?php echo number_format($col['monto']);?></td>
							</tr>
							<?php endwhile; ?>

	                	</tbody>

						<tfoot>
			                <tr>
			                    <th class="text-left">Total de Activos</th>
			                    <th class="text-right"><?php echo number_format($GetTotalActivosQuery['monto']) ;?></th>      
			                </tr>
		                </tfoot>

								
	                		 <?php// } ?>   
						
		                
	           			</table>
						  
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
                </div>
             
               
			<div class="col-lg-6">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                           <i class="fa fa-minus"></i> <?php echo $Liabilities ;?>
                            
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
							     
                            <div class="">
                                <table class="table table-hover table-striped" id="assetsdata">
                                    <thead>
        			                <tr>
        			                    <th class="text-left"><?php echo $Account ;?></th>
        			                    <th class="text-right"><?php echo number_format($Amount) ;?></th>
        			                </tr>
			                     </thead>

	                	<tbody>
							<!-- pasivo -->
							<tr>
								<th><?php echo $GetPasivoCorrienteQuery['CategoryName']; ?></th>
								<th></th>
							</tr>

							<?php while($col = mysqli_fetch_assoc($GetPasivoCorrienteQueryc)): ?>
							<tr>
								<td class="text-left"><?php echo $col['AccountName'];?></td>
								<td class="text-right"><?php echo number_format($col['monto']);?></td>
							</tr>
							<?php endwhile; ?>

							<tr>
								<td class="text-left"><i>Total de pasivos corrientes<i></td>
								<td class="text-right"><i><?php echo number_format($GetPasivoCorrienteTotalQuery['monto']); ?><i></td>
							</tr>

							<!-- pasivo no corriente -->
							<tr>
								<th><?php echo $GetPasivoNOCorrienteQuery['CategoryName']; ?></th>
								<th></th>
							</tr>

							<?php while($col = mysqli_fetch_assoc($GetPasivoNoCorrienteQueryc)): ?>
							<tr>
								<td class="text-left"><?php echo $col['AccountName'];?></td>
								<td class="text-right"><?php echo number_format($col['monto']);?></td>
							</tr>
							<?php endwhile; ?>

							<tr>
								<td class="text-left"><i>Total de pasivos no corrientes<i></td>
								<td class="text-right"><i><?php echo number_format($GetPasivoNoCorrienteTotalQuery['monto']); ?><i></td>
							</tr>


							<!-- capital -->
							<tr>
								<th><?php echo $GetCapitalQuery['CategoryName']; ?></th>
								<th></th>
							</tr>

							<?php while($col = mysqli_fetch_assoc($GetCapitalQueryc)): ?>
							<tr>
								<td class="text-left"><?php echo $col['AccountName'];?></td>
								<td class="text-right"><?php echo number_format($col['monto']); ?></td>
							</tr>
							<?php endwhile; ?>
							
							<tr>
								<td class="text-left"><i>Total de capital<i></td>
								<td class="text-right"><i><?php echo number_format($GetCapitalTotalQuery['monto']); ?><i></td>
							</tr>

	                	</tbody>

						<tfoot>
			                <tr>
			                    <th class="text-left">Total de Pasivos</th>
			                    <th class="text-right"><?php echo number_format($GetPasivosTotalQuery['monto']);?></th>      
			                </tr>
		                </tfoot>

								
	                		 <?php //} ?>   
						
	           			</table>
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
						
                    </div>
                    <!-- /.panel -->
					<?php if($GetPasivosTotalQuery['monto'] !== $GetTotalActivosQuery['monto'] ): ?>
						<script>
							$(document).ready(function(){
								$('#dif').modal('show'); 
							});
						</script>
						<div class="alert alert-danger">
							Existe una diferencia de <?php echo number_format($CountTotals) ; ?> pesos. <a class="alert alert-link" href="index.php?page=Transaction">Balancear la cuenta</a>
						</div>
						<?php elseif($GetPasivosTotalQuery['monto'] == 0 || $GetTotalActivosQuery['monto'] == 0): ?>
							No hay nada para mostrar
						<?php else: ?>
						<a onclick="window.print()"  class="btn white btn-success " data-toggle="modal"><i class="fa fa-download"></i> <?php echo $DownloadBalanceSheetPDF; ?></a>		
						<?php endif; ?>
                </div>
	
               
                <!-- /.col-lg-4 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->


	<div class="modal fade" id="dif" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Alerta</h4>
                </div>
                <div class="modal-body">
					Existe una diferencia entre los pasivos y activos, favor de corregir las transaciones
                </div>
                <div class="modal-footer">
					<a class="btn btn-primary" href="index.php?page=Transaction">
						<span class="">
							Balancear la cuenta
						</span>
					</a>
            </form>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
   

	<script>

	$(document).ready(function (){
		// var sum = 0;
	// // iterate through each td based on class and add the values
	// $(".activo").each(function() {
	// 	var value = $(this).text();
	// 	// add only if the value is number
	// 	if(!isNaN(value) && value.length != 0) {
	// 		sum += parseFloat(value);
	// 	}
	// });

	// $('#activoTotal').text(sum)
	// console.log(sum)
		
	});

	</script>