<?php



// Menu language
$Dashboard		    	= "Dashboard";
$Transaction			= "Transacción";
$Incomes				= "Ingresos";
$Reports				= "Reportes";
$ManageBudget			= "Manage Budget";
$ManageCategories		= "Administrar Categorías";
$ManageAccount			= "Administrar Cuenta";
$Expenses				= "Gastos";
$Settings				= "Configuración";
$Logout					= "Logout";

//Pages language
$Welcome				= "Bienvenido";
$Name					= "Nombre";
$Amount					= "Monto";
$Category				= "Categoría";
$Account				= "Cuenta";
$Level				    = "Procedencia";
$Date					= "Fecha";
$Description			= "Descripción";
$HistoryofBudget		= "History of Budget";
$AssetReports			= "Reporte de Activos";
$Asset                  = "Activo";
$Liabilities            = "Pasivo";
$HistoryofAssets		= "Historial de ingresos";
$AssetsbyAccount		= "Activos por Cuenta";
$Action					= "Acción";
$TotalIncomeThisMonth	= "Ingreso Total de este mes";
$TotalIncomeToday		= "Total de ingresos de hoy";
$DownloadIncomeReports	= "Descargar Reportes de Ingresos";
$BudgetFor				= "Budget for";
$BudgetInformation		= "Budget Information";
$ListCategory			= "Lista de Categoría";
$AddNewCategory			= "Agregar Nueva Categoría";
$ListAccount			= "Lista de Cuentas";
$AddAccount				= "Agregar Nueva Cuenta";
$ExpenseReports			= "Reportes de Gastos";
$HistoryofExpense		= "Historial de gastos";
$ExpenseByCategory		= "Gastos por categoria";
$TotalExpenseThisMonth  = "Total de gastos estes mes";
$TotalExpenseToday		= "Total de gastos hoy";
$DownloadExpenseReports = "Descargar reporte de gastos";
$DownloadExpenseCSV 	= "Descargar gastos CSV";
$DownloadIncomeCSV 		= "Descargar ingresos CSV";
$ChangeAccountProfile	= "Cambiar cuenta de perfil";
$Emails					= "Email";
$Currencys				= "Moneda";
$Company				= "Compania";
$CreateAnAccount		= "Crea una cuenta";
$UserSign				= "Inicio de Sesión";
$FirstNames				= "Nombre";
$LastNames				= "Apellido";
$Passwords				= "Contrasena";
$RepeatPassword			= "Repetir Contrasena";
$BudgetProgressOn		= "Budget Progress On";
$Out					= "Out";
$Outs					= "Out";
$RemainingBudget		= "Remaining Budget";
$CurrentExpense			= "Gasto actual de este mes";
$CurrentIncome			= "Ingreso actual de este mes";
$TotalExpenseDashboard  = "Tus gastos totales";
$CurrentBalance			= "Tu balance total actual";
$TenIncome				= "Ultimos 10 ingresos";
$TenExpense				= "Ultimos 10 gastos";
$Title					= "Titulo";
$Amounts				= "Monto";
$Exceeds				= "Over Budget";
$ReportsExpenseIncome	= "Reportes Gastos/Ingresos";
$ReportsExpenseIncomeM	= "Reportes Gastos/Ingresos este mes";
$BudgetForCategory		= "Budget for Category";
$Search					= "Buscar...";
$AccountMessage			= "<h4>Warning!</h4> Monto en esta cuenta sera borrado de la base de datos.";
$ManageExpense			= "Administrar Gastos";
$ManageIncome			= "Administrar Ingresos";
$Budgetss				= "Budget";
$IncomeVsExpense		= "Ingresos vs Gastos";
$OverallReport			= "Overall Report";
$ReportByMonth			= "Reporte este mes";
$ReportByDay			= "Reporte de hoy";
$ReportTotalIncome		= "Total ingreso :";
$ReportTotalExpense		= "Total gasto :";
$ReportIncomeExpense	= "Ingreso - Gasto :";
$Today 					= "Hoy";
$Months 				= "Mes";
$Overall 				= "Total";
$CalenderIncome			= "Calendario de Ingresos";
$CalenderExpense		= "Calendario de Gastos";

//MessageBox
$AreYouSure				= "Estas seguro/a de borrar este item?";
$ThisItem				= "Este item sera borrado del historial de base de datos.";
$EditThisCategory		= "Editar esta Categoría?";
$EditThisAccount		= "Editar esta Cuenta?";
$EditThisBudget			= "Edit this Budget?";

$CategoryIncome			= "Categorías de Ingresos";
$CategoryExpense		= "Categorías de Gastos";
$ProfileSettings		= "Configuración de Perfil";
$ReportsGraphs			= "Reportes & Graficos";
$IncomeReportsM 		= "Reporte de Ingresos";
$ExpenseReportsM		= "Reporte de Gastos";
$ExpenseIncomeReports   = "Balance General";
$BudgetsM				= "Budgets";
$AccountBalance			= "Balance de Cuentas";
$NewTransaction			= "Nueva Transacción";
$NewAccount				= "Nueva Cuenta";
$ManageCategoriesIncome	= "Administar Categorías de Ingresos";
$ManageCategoriesExpense	= "Administar Categorías de Gastos";
$NewBudgets				= "Add New Budget";
$Amounts 				= "Monto";
$IncomeCalender			= "Calendario de Ingreso";
$ExpenseCalender		= "Calendario de Gasto";
$IncomeSummary			= "Resumen de Ingresos";
$ExpenseSummary			= "Resumen de Gastos";
//For Report
$TotalExpenseReport		= "Total Gasto: "; 
$TotalIncomeReport		= "Total Ingreso: "; 


//Button for all pages
$Yes					= "Si";
$Save					= "Guardar";
$Cancel					= "Cancelar";
$RegisterAnAccount		= "Registar una Cuenta";
$SignIn					= "Iniciar Sesión";
$SaveIncome				= "Guardar Ingreso";
$EditIncome				= "Editar Gasto";
$DeleteIncomes			= "Borrar Ingreso";
$SaveExpense			= "Guardar Gasto";
$EditExpense			= "Editar Gasto";
$DeleteExpenses			= "Eliminar Gasto";
$SaveBudget				= "Save Budget";
$EditBudget				= "Edit Budget";
$DeleteBudgets			= "Delete Budget";
$SaveCategory			= "Guardar Categoría";
$EditCategory			= "Editar Categoría";
$DeleteCategories		= "Eliminar Categoría";
$SaveAccount			= "Guardar Cuenta";
$EditAccount			= "Editar Cuenta";
$DeleteAccounts			= "Borrar Cuenta";
$ViewDetails			= "Ver Detalles";



//notification message for all pages
$SaveMsgIncome			= "Este ingreso ha sido guardado satisfactoriamente";
$UpdateMsgIncome		= "Ingreso ha sido actualizado satisfactoriamente";
$UpdateMsgExpense		= "Gasto ha sido actualizado satisfactoriamente";
$SaveMsgExpense			= "Gasto ha sido guardado satisfactoriamente";
$LoginError				= "Login fallo , Por favor verifica tu usuario/contrasena.";
$EmailEmpty				= "Por favor entra tu direccion de correo electronico";
$PasswordEmpty			= "Por favor ingesa tu contrasena";
$SignUpEmpty			= "Todos los campos son requeridos.";
$PwdNotSame				= "Las contrasenas no son iguales.";
$SuccessAccount			= "La cuenta ha sido creada satisfactoriamente, Por favor " .'<a href="login.php">Login!</a>';
$AlreadyRegister		= "Cuenta ha sido registrada, Por favor  " .'<a href="login.php">Login!</a>';
$SuccessAccountUpdate	= "Cuenta ha sido actualizado satisfactoriamente.";
$DeleteIncome			= "Ingreso ha sido borrada satisfactoriamente.";
$DeleteCategory			= "Categoria ha sido borrada satisfactoriamente.";
$SaveMsgCategory		= "Categoria ha sido guardadada satisfactoriamente.";
$UpdateMsgCategory		= "Categoria ha sido actualizada satisfactoriamente";
$DeleteAccount			= "Cuenta ha sido borrada satisfactoriamente.";
$SaveMsgAccount			= "La cuenta ha sido guardada satisfactoriamente.";
$UpdateMsgAccount		= "La cuenta ha sido actualizada satisfactoriamente";
$SaveMsgBudget			= "Budget has been saved sucessfully.";
$AlreadyBudget			= "Sorry! Budget for category has been added before.";
$DeleteBudget			= "Budget has been deleted sucessfully.";
$UpdateMsgBudget		= "Budget has been updated sucessfully";
$DeleteExpense			= "Gasto ha sido borrado satisfactoriamente.";
$NegativeAmount			= "No esta permitido monto negativo";
$MessageEmpty			= "Aglun campo es requerido.";

//Balance sheet
$DownloadBalanceSheetPDF = "Descargar Balance General";

?>
